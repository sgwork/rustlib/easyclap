# Welcome to easyclap for Rust

[![](https://badgen.net/github/license/sguerri/rust-easyclap)](https://www.gnu.org/licenses/)
[![](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](#)

> Easy clap commands

TODO description

**Main features**
* TODO

**Roadmap**
* TODO

---

- [Welcome to easyclap for Rust](#welcome-to-easyclap-for-rust)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Build](#build)
  - [Dependencies](#dependencies)
  - [Author](#author)
  - [Issues](#issues)
  - [License](#license)

## Installation

## Usage

## Build

## Dependencies

- [clap](https://crates.io/crates/clap)

## Author

Sébastien Guerri - [github page](https://github.com/sguerri)

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://github.com/sguerri/rust-easyclap/issues). You can also contact me.

## License

Copyright (C) 2023 Sebastien Guerri

easyclap is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

easyclap is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with easyclap. If not, see <https://www.gnu.org/licenses/>.
